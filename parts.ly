% vim: sts=2 sw=2 expandtab

start_chaosa      = { s1*0                      | } % 0:25
start_chaosb      = { \start_chaosa      \skip 4*151 | } % 1:15
start_space       = { \start_chaosb      \skip 4*153 | } % 2:06
start_lifea       = { \start_space       \skip 4*96  | } % 3:07
start_lifeb       = { \start_lifea       \skip 1*20  | } % 4:01
start_adventure   = { \start_lifeb       \skip 1*15  | } % 4:41
start_home        = { \start_adventure   \skip 1*6/8*71  | } % 6:05
start_celebration = { \start_home        \skip 1*6/8*20  | } % 6:29
start_meaning     = { \start_celebration \skip 1*6/8*86  | } % 7:52
start_finale      = { \start_meaning     \skip 4*78  | } % 8:35
end               = { \start_finale      \skip 4*161  | } % end

colin = {
  \override Accidental.color = #(rgb-color 0.13 0.29 0.53)
  \override AccidentalCautionary.color = #(rgb-color 0.13 0.29 0.53)
  \override Beam.color = #(rgb-color 0.13 0.29 0.53)
  \override Dots.color = #(rgb-color 0.13 0.29 0.53)
  \override NoteHead.color = #(rgb-color 0.13 0.29 0.53)
  \override PhrasingSlur.color = #(rgb-color 0.13 0.29 0.53)
  \override Rest.color = #(rgb-color 0.13 0.29 0.53)
  \override Script.color = #(rgb-color 0.13 0.29 0.53)
  \override Slur.color = #(rgb-color 0.13 0.29 0.53)
  \override Staff.OttavaBracket.color = #(rgb-color 0.13 0.29 0.53)
  \override Stem.color = #(rgb-color 0.13 0.29 0.53)
  \override Tie.color = #(rgb-color 0.13 0.29 0.53)
  \override TupletBracket.color = #(rgb-color 0.13 0.29 0.53)
  \override TupletNumber.color = #(rgb-color 0.13 0.29 0.53)
}
lauren = {
  \override Accidental.color = #(rgb-color 0.47 0.21 0)
  \override AccidentalCautionary.color = #(rgb-color 0.47 0.21 0)
  \override Beam.color = #(rgb-color 0.47 0.21 0)
  \override Dots.color = #(rgb-color 0.47 0.21 0)
  \override NoteHead.color = #(rgb-color 0.47 0.21 0)
  \override PhrasingSlur.color = #(rgb-color 0.47 0.21 0)
  \override Rest.color = #(rgb-color 0.47 0.21 0)
  \override Script.color = #(rgb-color 0.47 0.21 0)
  \override Slur.color = #(rgb-color 0.47 0.21 0)
  \override Staff.OttavaBracket.color = #(rgb-color 0.47 0.21 0)
  \override Stem.color = #(rgb-color 0.47 0.21 0)
  \override Tie.color = #(rgb-color 0.47 0.21 0)
  \override TupletBracket.color = #(rgb-color 0.47 0.21 0)
  \override TupletNumber.color = #(rgb-color 0.47 0.21 0)
}

foureqfourdot = \markup \concat {
  (
  \smaller \general-align #Y #DOWN \note #"4" #1
  " = "
  \smaller \general-align #Y #DOWN \note #"4." #1
  )
}
rit = \markup \italic "rit."
atempo = \markup \italic "a tempo"

piston =
#(define-music-function
   (parser location pistonNumber)
   (markup?)
   #{
     \mark \markup \box \fontsize #2 \number #pistonNumber
     \bar "||"
   #})

global = << \end {
  %\set Score.skipTypesetting = ##t
  \numericTimeSignature
  \piston "1"
  \key g \major
  \tempo 4=172
  \time 14/4 s1*14/4*1
  \time 13/4 s1*13/4*1
  \time 4/4 s1*4/4*1
  \time 5/4 s1*5/4*1
  \time 4/4 s1*4/4*1
  \time 5/4 s1*5/4*2
  \time 2/4 s1*2/4*1
  \time 14/4 s1*14/4*1
  \time 12/4 s1*12/4*1
  \time 4/4 s1*4/4*1
  \time 5/4 s1*5/4*4
  \time 15/4 s1*15/4*1
  \time 6/4 s1*6/4*3
  \time 5/4 s1*5/4*2
  \time 6/4 s1*6/4*2

  \piston "2"
  \key a \minor
  \time 6/4 s1*6/4*3
  \time 5/4 s1*5/4*1
  \time 7/4 s1*7/4*2
  \piston "3"
  \time 5/4 s1*5/4*14
  \piston "4"
  \time 8/4 s1*8/4*1
  \time 7/4 s1*7/4*1
  \time 6/4 s1*6/4*1
  \time 5/4 s1*5/4*1
  \time 4/4 s1*4/4*1
  \time 6/8 s1*3/4*2
  \tempo 4=88
  \time 4/4 s1*4/4*1

  \piston "5"
  \key e \minor
  \time 6/4 s1*6/4*7
  \time 5/4 s1*5/4*2
  \time 4/4 s1*4/4*3
  \time 4/4 s1*4/4 ^\rit
  \time 4/4 s1*4/4 ^\atempo
  \time 4/4 s1*4/4*6

  \key es \minor
  s1*20

  s1*5
  \key c \major
  s1*8
  \key d \major
  s1
  s2
  s2 ^\rit

  %\set Score.skipTypesetting = ##f % Move this to work on a specific section
  \piston "6"
  \tempo 4.=100
  \time 6/8 s1*6/8*61
  \key des \major
  s1*6/8*6
  \piston "7"
  s1*6/8*4

  s1*6/8*20

  %\set Score.skipTypesetting = ##t % Move this to work on a specific section
  \piston "8"
  \tempo 4.=132
  \key a \major s1*6/8*48
  \piston "9"
  s1*6/8*8
  \tempo 4.=112
  \key es \major s1*6/8*30

  \tempo \foureqfourdot 4=112
  \time 4/4 s1*3
  s2 s2^\rit
  \piston "10"
  \pageBreak
  \time 2/4 s1*2/4*1
  \time 4/4 s1*15^\atempo

  \piston "11"
  \tempo 4=180
  \time 7/4 s1*7/4*8
  \key e \major s1*7/4*9
  \piston "12"
  s1*7/4*2
  s1*7/4*2
  \time 5/4 s1*5/4*1
  \time 9/8 s1*9/8*2
  \bar "|."
} >>

chaosa_flutes = \relative c'' {
  \start_chaosa \change Staff = "choir"
  R4*52 |
  \lauren
  r4 r r r r <e g> r <fis a> r <b, d> <c e> r <d fis> r |
  r4 r <e g> r r <fis a> <d fis> r <d fis> <c e> r2 |
  R1 R4*20 |
  r4 r r <d fis>-^ r <a g'>-^ r <d a'>-^ r r <b d>-^ r8 <d fis>-^ r4 <b e>-^ r |
  R4*6*3 R4*5*2 R4*6*5 R4*5 |
  \colin
  r2 r4 r4. r4 <e g>8[ q e] <d f> r r4 r4 r4. r4 r4. |
  R4*15 |
  \lauren
  r4. r4. r4 a''16 g f e |
  d[ c b a g f] e[ d c b a8] r4 r |
  R4*10 |
  g8 r r g r g g a bes g a8 r r a r bes a g f r |
}
chaosa_strings = \relative c' {
  \start_chaosa \change Staff = "swell"
  \colin
  r4 a8 c b d c e d fis e g fis a e fis c e b d a c g b r4 r |
  r4 a8 c b d c e d fis e g fis a g b r a fis g e fis d e r4 |
  R1 s1*5/4 s1 s1*12/4 |
  r4 a,8 c b d c e d fis e g fis a e fis c e b d a c g b r4 r |
  r4 a8 c b d c e d fis e g fis a g b r a fis g e fis r4 |
  R1 | s4*20 |
  \change Staff = "positiv"
  \absolute {
    e'8^"POS(3)" e b e a e
    e' e b e a e
    e' b e a d' a
    e' b e a d' a
    e' b e a d' a
  }
  \change Staff = "great"
  \lauren r4 fis'8 fis fis fis fis g a r d, e fis r b, b b b b cis d cis b r |
  r4 fis'8 fis fis fis fis g a r d, e fis r b, d r cis a b g fis |
  \change Staff = "swell"
  \colin
  r4 a,8 c b d c e d g |
  fis8 r d fis e g fis a g b a cis |
  d8 r r4 r2 r |
  \lauren
  r4 e8 e e e e f g r c, d e r a, a a a a b c b a r |
  r4 e'8 e e e e f g r c, d e r a, c r b g a f e |
  d[ f bes d] f d e c r f[ d] e[ c] r |
  f,[ bes d f] a f g e r a[ f] g[ e c] |
  \repeat unfold 14 {
    d8 r r d, r d d' r d, r |
  }
  d8 r d f e g f a g bes a c bes d c e |
  f8 r f, a g bes a c bes d c e d f |
  g8 r a, c bes d c es d f es g |
  as8[ des,] f[ des] g[ es as!] a[ f ges] |
  bes[ fis g] b[ gis c] cis16 a b cis |
  d8 r4 r4. |
}
chaosa_brass = \relative c' {
  \start_chaosa \change Staff = "swell"
  s4*14 s4*13 s1 |
  \lauren
  r4 <c e>8[ <d fis>] <e g>[ <d fis> <e g>] <fis a>[ <e g> <d fis>]
  <e g>8[ <d fis> <e g>] <fis a>[ <e g> <d fis>] <c e> r |
  r4 <c e>8[ <d fis>] <e g>[ <d fis> <e g>] <fis a>[ <e g> <d fis>]
  <e g>8[ <d fis> <e g>] <g b>[ <fis a> <e g>] <d fis>[ <e g> <fis a> <g b>] |
  R2 s4*14 s4*12 s1 |
  r4 <c, e>8[ <d fis>] <e g>[ <d fis> <e g>] <fis a>[ <e g> <d fis>]
  <e g>8[ <d fis> <e g>] <fis a>[ <e g> <d fis>] <e g>[ <d fis> <c e>] r |
  r4 <c e>8[ <d fis>] <e g>[ <d fis> <e g>] <fis a>[ <e g> <d fis>]
  <e g>8[ <d fis> <e g>] <fis a>[ <e g> <d fis>] <c e>[ <d fis> <e g> <fis a>] |
  <g b>8 \colin r r4 r r2. r2. r2.
  r4 \times 2/3 {b,8 c cis} \times 2/3 {d dis e} |
  <e fis>1. R1. |
  \times 2/3 {<e fis>4-- q-- q--} q1 R1*5/4 |
  s4*59
  \change Staff = "great"
  <d f>4-. r8 <d f>4-. r8 <e g>4-. <f a>-. |
  <g bes>4-. r8 <g bes>4-. r8 <g bes>4-. <a c>-. |
  <f a>4.~ <f a>4.~ <f a>8 r <e g>4-. |
  <f a>4-. r8 r4. r4 r |
  <c f>1*5/4 <d f> <e~ g> <e f a>1 r4 |
  R1*5/4*3 |
  \change Staff = "positiv"
  <f d>8 r r q r q <g e> r <a f> r <g e>8 r r q r <e d> e f g e |
  d8 r r4 r2 s1 |
  r4 r r \appoggiatura <dis fisis>8 \times 2/3 {<e gis>4 q q} q4 r |
  s4*21 |
  \change Staff = "swell"
  \colin
  r4 \clef "bass" \times 2/3 {<es, g bes>16-. q-. q-.} q8-. r2 |
}
chaosa_hits = \relative c'' {
  \start_chaosa \change Staff = "great"
  \lauren
  <a e c>4 r r r r r r r r r r r r r |
  <a e c>4 r r r r r r r r r r r r <a c> r8 <b d>4 r8 <d fis>4 |
  R4*21 |
  <a e c>4 r r r r r r r r r r r r r |
  <a e c>4 r r r r r r r r r r r <a c> r8 <b d>4 r8 <d fis>4 |
  R4*20 s4*15 s4*6*3 s4*5 |
  \lauren
  <a e c>4-^ r r r r <b fis d>4-^ r r2 r |
  <d a f>4-^ r r2 r |
  R4*6*3 R4*5 |
  d,8[ f bes d] f d e c r f[ d] e[ c] r |
  d,[ f bes d] f d e c r f[ d] e[ c] a |
  s4*70 |
  \colin
  s1 r2 r4 <e a,> |
  <f a,>4 r r r r r <f d> |
  <g es>4 r r r r8 <g es>4. |
  <as f des>4 r r4. <ces f, des> |
  <bes g es>4 r8 r4. <a g es>4 |
  \clef "bass"
  a,8 d, e f g a |
  f8 e f g a cis |
  d8-> r r4 r2 |
}
chaosa_trumpet = \relative c''' {
  \start_chaosa \change Staff = "bombarde" \clef "treble"
  R4*78 |
  \lauren
  a4 r8 b4 r8 d4 |
  R4*20 |
  r4 r r \ottava 1 fis-^ r g-^ r a-^ r r d,-^ r8 fis8-^ r4 e-^ \ottava 0 r |
  R4*6*3 R4*5*2 |
  r2 \times 2/3 {<d, fis>4-. <d fis d'>-. q-.} \times 2/3 {q-. q-. q-.} |
  <d f d'>4-^ r4 r2 r2 |
  R4*97 |
  f4-. r8 f4-. f8-. g4-. a-. |
  g4-. r8 g4-. e8-. e-. f-. g-. e-. |
  d4-. r r2 r1 |
  R4*7 |
  \colin
  r4 r \times 2/3 {r <d a'> q} \times 2/3 {q q q} |
  <e gis>8 r r4 r4. r4. |
  R4*7 |
  \lauren
  f8 e f g a cis |
  d8 r r4 r2 |
}
chaosa_low = \relative c {
  \start_chaosa \change Staff = "positiv" \clef "bass"
  \colin
  r4 a8 a a a a a a a a a a a a a a a a a a a a a r2 |
  r4 a8 a a a a a a a a a a a a a a a a a a a a a r4 |
  R1 |
  R1*5/4 R1*4/4 R1*5/4*2 R2 |
  r4 a8 a a a a a a a a a a a a a a a a a a a a a r2 |
  r4 a8 a a a a a a a a a a a a a r a a a a a r4 |
  R1 R4*20 |
  s4*15 | R4*6*3 R4*5 |
  \times 2/3 {a4 a a} \times 2/3 {a a a} \times 2/3 {a a8}
  \times 2/3 {b4 b b} \times 2/3 {b b b} \times 2/3 {b b b}
  \clef "treble"
  \transpose c c' \absolute {
    d'8 d a d g d d' d a d g d
    d'8 d a d g d d' d a d g d
    d'8 d d' a g d d' d a d g d
    d'8 d a d g d d' d a d g d
    d'8[ d a d g d] d'[ d a d]
  }
  R4*39 |
  c'4. c4 c8 d4 e f4. f4 a,8 a4 g |
  e'4. r4. r2 R1*5/4 |
  c'4 d8 c4 f,8 bes8 c bes g a4 bes8 a4 d,8 g a g e |
  f2.~ f4 r |
}
chaosa_bass = \relative c {
  \start_chaosa \change Staff = "pedal" \clef "bass"
  \colin
  a4 r r r r r r r r r r r r a4 |
  a4 r r r r r r r r r r r r8 a r4 a8 r4 e8 r4 |
  a1*5/4^"-Bom" ~ a1 c1*5/4~ c | R2^"+Bom" |
  a4 r r r r r r r r r r r r a4 |
  a4 r r r r r r r r r r r8 a r4 a8 r4 e8 r4 |
  d'1*5/4^"-Bom"~ d fis,~ fis |
  e4 r r r2.^"+Bom" r2. r2. r2. |
  e'8 r r e r r r e e r b r e r r e r r r e e r e r |
  e8 r r e r r r e e r b r e r r e r r r e e r |
  a,8 r r4 r r r b8 r r4 r r r r |
  d8 r r4 r r2. |
  d4-. a-. d,-. r2. r2. r4 d'-. a-. |
  d,4-. a'-. d-. r2. r4 r r r d-. |
  bes4-. r r4 r4. r4 r4. bes4-. r r4 r4. r4 r4. |
  \repeat unfold 5 {
    d4 r8 r4. d8 r d r |
  }
  c4 r8 r4. c8 r c r |
  bes4 r8 r4. bes8 r bes r |
  d4 r8 r4. d8 r d r |
  d4 r8 r4. d8 r d^"+Pos to Pd" r |
  bes4 r8 r4. g8 r g r |
  f4 r8 r4. e8 r e r |
  bes'4 r8 r4. bes8 r bes r |
  a4 r8 r4. a8 r a r |
  a4 r8 r4. a8 r a r |
  d8-> r r d r r d r r d r d d r a-> r |
  d8-> r r d r d d r r r d r a-> r |
  d8-> r r d d d d8 r r a4.-> |
  d8-> r r d r d r a4.-> |
  d8-> r r d[ d d] a-> a |
  d8-> r r r r a8 |
  d8-> r r a-> d8-> a-> |
  d8-> r r4 r2 |
}

space_arp = {
  \start_space \change Staff = "positiv"
  R4*6
  \lauren
  d''8 a' d' a d' a' d'' a'' d'' a' d'' a' d' a d' a' d' a d' a' d'' a'' d'' a' |
  c'8 g' c'' g'' c''' g'' c'' g' c' g' g' c'' |
  d' a' d' a' d'' a'' d' a' e' a' e' a' |
  g' g c' e' d'' e' d'' e' c'' d'' e'' g' |
  b'4 \times 2/3 {fis8 gis b} \times 2/3 {e' fis' gis'} \times 2/3 {b' gis' fis'}
  \times 2/3 {e'8 b gis} \times 2/3 {fis e b,} |
  \times 2/3 {r4 a8} \times 2/3 {b c' e'} \times 2/3 {a' b' c''}
  \times 2/3 {e''8 c'' b'} \times 2/3 {a' e' c'} |
  r4 \times 2/3 {fis8 gis b} \times 2/3 {e' fis' gis'}
  \times 2/3 {e''8 b' gis'} \times 2/3 {e' b e} |
  \times 2/3 {r8 e a} \times 2/3 {b c' e'} \times 2/3 {a' b' c''} \times 2/3 {e'' c'' a'} |
  \times 2/3 {r8 a d'} \times 2/3 {e' fis' c''} \times 2/3 {e'' fis'' a''} \times 2/3 {c''' r4} |
  \times 2/3 {r8 bes d'} \times 2/3 {e' g' bes'} \times 2/3 {d'' r4} r |
  \times 2/3 {r8 fis g} \times 2/3 {fis g b} \times 2/3 {d' fis' g'} \times 2/3 {b' r4} |
  e8-. fis-. b-. d'-. fis'-. b'-. d''-. e''-. |
  r8 e-. b-. c'-. e'-. a'-. c''-. e''-. |
  r8 fis-. b-. d'-. fis'-. gis'-. b'-. e''-. |
  r8 e-. b-. c'-. e'-. a'-. c''-. e''-. |
  r8 fis-. a-. c'-. e'-. bes-. d'-. a'-. |
  gis'8-. e'-. f'-. d'-. bes-. as-. f-. d-. |
  r8 fis-. a-. b-. dis'-. fis'-. a'-. b'-. |
}
space_bass = {
  \start_space \change Staff = "pedal"
  \colin
  d,1.~ d,~ d, e,~ e, c2. a, e,1. |
  a,4-. r r2. e,4-. r r2. |
  a,4-. r r2 d,4-. r r2 c,4-. r r2 g,4-. r r2 |
  e,4-. r r2 |
  a,4-. r r2 |
  e,4-. r r2 |
  a,4-. r r2 |
  d,4-. r c4-. r |
  bes,4-. r r2 |
  b,!4-. r r2 |
}
space_oboe = {
  \start_space \change Staff = "swell"
  s4*42 |
  \clef "treble"
  \colin
  r2^"SW(3)" c''4 g'' fis''
  e'' b'1
  r4 c'' g'' fis''
  e''2 b''8 a'' fis'' d''
  fis''2~  fis''8 g'' e'' fis''
  b''4. d''8 d''4 a''
  fis''2 s2 |
}
space_strings = \relative c {
  \start_space \change Staff = "swell"
  \clef "bass"
  \colin
  <d a>1.~ q~ q |
  e2.~ e4 fis g |
  fis2.~ fis4 g a |
  g2 d'4~ d2 e4 |
  b1. |
  s4*26 |
  s2 b'^"SW(2)"~ |
  b4 c g' fis e b2.~ |
  b4 \breathe c g' fis e2 a8 g e c |
  e4 f4. g8 e f ges4 des'2 es4 |
  bes1~ bes~ bes~ bes2. r4 |
}
space_strings_ii = \relative c' {
  \start_space \change Staff = "choir"
  s4*42
  \colin
  <b g>2.~ q2 |
  <b gis>2.~ q2 |
  <c a>1~ q |
  <d~ bes>1 <d b!~> |
  b1 |
}
space_quiet_background = \relative c' {
  \start_space \change Staff = "choir"
  s4*12 |
  \clef "bass"
  \colin
  <d a>1. <c g c,> |
  <a d,>1. <e g> <e b'> |
}
space_clarinet = {
  \start_space \change Staff = "choir"
  s4*92
  \clef "treble"
  \colin
  ges'4^"CH(1)" des''2 ees''4
  bes'1~
  bes'2 \lauren r4 des''
  aes'2. bes'4~
  bes' ees'2 ges'8 f'
  ees'2
}

lifea_arp = {
  \start_lifea \change Staff = "positiv"
  \lauren
  r8 ges-. as-. bes-. des'-. es'-. ges'-. as'-. |
  r8 ges-. as-. bes-. des'-. es'-. ges'-. as'-. |
  r8 as-. bes-. des'-. es'-. ges'-. as'-. bes'-. |
  r8 bes-. des'-. es'-. ges'-. as'-. bes'-. des''-. |
}
lifea_marimba = {
  \start_lifea \change Staff = "positiv"
  s1*3 |
  \colin
  \voiceTwo s2 ges16 as bes des' bes as ges as | \oneVoice
  r8^"POS(2)" bes-. des'-. as-. des'16 ees' ges' aes' ges' ees' des'8 |
  r8 bes-. des'-. as-. ges16 aes bes des' bes aes ges8 |
  r8 bes-. des'-. as-. des'16 ees' ges' aes' ges' ees' des'8 |
  r8 bes-. des'-. as-. ges16 aes bes des' bes aes ges8 |
  r8 bes-. es'-. des'-. ges'16 aes' bes' aes' ges' ees' des' bes |
  r8 bes-. des'-. as-. bes16 des' ees' des' bes aes ges8 |
  r8 bes-. es'-. des'-. ges'16 aes' bes' aes' ges' ees' des' bes |
  r8 bes-. des'-. as-. bes16 des' ees' des' bes aes ges8 |
  r bes des' aes des'16 ees' ges' aes' ges' ees' des' bes |
  r8 bes des' aes ges16 aes bes des' bes aes ges8 |
  r bes des' aes des'16 ees' ges' aes' ges' ees' des' bes |
  r8 bes des' aes ges16 aes bes des' bes aes ges8 |
  r bes ees' des' ges'16 aes' bes' aes' ges' ees' des' bes |
  r8 bes des' aes bes16 des' ees' des' bes aes ges8 |
  ees ces' ees' ces' ges'16 aes' bes' aes' ges' ees' des' bes |
  r8 bes des' aes ges16 aes bes des' bes aes ges8 |
}
lifea_bass = {
  \start_lifea \change Staff = "pedal"
  \colin
  es,4-. r r2 |
  ces4-. r r2 |
  as,4-. r r2 |
  ces4-. r r2 |
  es,4-. r r4. es,8-. |
  es,4-. r r2 |
  es,4-. r r4. es,8-. |
  es,4-. r r2 |
  ces4-. r r2 |
  ges,4-. r r2 |
  as,4-. r r2 |
  bes,4-. r r8. bes,16-. bes,4-. |
  es,4-. r r2 |
  es,4-. r r2 |
  es,4-. r r2 |
  es,4-. r r2 |
  ces4-. r r2 |
  ges,4-. r r2 |
  as,4-. r r2 |
  bes,4-. r r2 |
}
lifea_melody = {
  \start_lifea \change Staff = "choir"
  s4*16
}
lifea_strings = \relative c'' {
  \start_lifea \change Staff = "swell"
  s4*16 |
  \lauren
  r8 bes des aes r2 |
  r8 bes des aes r2 |
  r8 bes des aes r2 |
  r8 bes des aes r2 |
  ees8 bes' ees des r2 |
  ees,8 bes' des aes r2 |
  ees8 bes' ees des r2 |
  ees,8 bes' des aes r2 |
  bes1^"SW(1)"~
  bes2 ges'4 f
  es2 bes~
  bes ges'4 f
  es2. bes'4
  as f des f
  ges2~  ges8 aes f ges
  bes1~
  bes2. r4
}
lifea_brass = \relative c' {
  \start_lifea \change Staff = "great"
  s4*48 |
  \clef "treble"
  \lauren
  bes1~ bes2 ges'4 f |
  es2 bes~ bes ges'4 f |
  <es as, ges>1 <es des bes> <es ces as> <f es des bes> |
}
lifea_low_strings = \relative c {
  \start_lifea \change Staff = "great"
  s4*32 |
  \clef "bass"
  \colin
  ces1 ges as bes2. r4 |
}

lifeb_bass = {
  \start_lifeb \change Staff = "pedal"
  \colin
  es,1 |
  es2 es es es |
  ces4-.^"build" \lauren ces'-. \colin bes,-. \lauren bes-. |
  \colin as,-. \lauren as-. \colin ges,-. \lauren ges-. |
  \colin
  d8 d d d d d d d
  d d d d d d d d
  c c c c d d d d
  b, b, c c c c a,4

  bes,4 bes, bes, bes,
  bes, bes, bes, bes,
  bes, bes, bes, bes,
  bes, bes, bes, bes,
  a,4 a, a, a, |
  a,4 a, \times 2/3 {a,8 a, a,} \times 2/3 {a, a, a,->} |
}
lifeb_flute = {
  \start_lifeb \change Staff = "bombarde"
  \lauren
  r2 r8 ees'^"harmonic flute" bes' ges'
  aes'4 aes'~ aes'8 \grace {c''8}  aes'4.~
  aes'2~  aes'8 ges' aes' ges'~
  ges'2. r4
}
lifeb_flute_ii = {
  \start_lifeb \change Staff = "choir"
  s1*5
  \lauren
  r2 r8^"CH(2)" d' c'' a'
  b'8. b'16~  b'8 b' b'8. c''16~  c''8 d''
  e''2 f''4.\( e''8
  d''4\) e''4.\( d''8 c''4\)
  d''1
}
lifeb_brass = {
  \start_lifeb \change Staff = "great"
  \clef "bass"
  \colin
  <es bes>1 es~ es |
  \clef "treble"
  es'1~
  << {\voiceTwo es'1}
    \new Voice {\voiceOne \colin ges'2 as'}
  >> \oneVoice |
  <e' a'>1 f' |
  e'2 f'4.\( e'8 d'4\) e'4.\( d'8 c'4\) |
  r4^"GT(1)" <bes d'>16 q q8 r8 q16 q q8 r |
  r4 <c' e'>16 q q8 r8 q16 q q8 r |
  r4 <bes d' f'>16 q q8 r8 q16 q q8 r |
  r4 <c' e' g'>16 q q8 \times 2/3 {r8 q q} \times 2/3 {q q q} |
  \lauren
  r4 <fis d'>2. <g e'>2~ \times 2/3 {q4 q8} \times 2/3 {q4 q8} |
}
lifeb_strings = \relative c' {
  \start_lifeb \change Staff = "swell"
  s1 |
  \lauren
  <c es as>1^"SW(2)" <des f as> |
  R1 |
  ces'1\trill |
  a1~ a2 r |
  <a, c e>2 <a d f> <g~ b d>4 <g c e>2 <f~ a c>4 |
  <f bes d>1 <g c e> <f bes d f> <g c e g>
  \times 2/3 {<fis' a>8^"SW(7)" <e gis> <fis a>} \times 2/3 {<e gis> <fis a> <e gis>}
  \times 2/3 {<fis a>8 <e gis> <fis a>} \times 2/3 {<e gis> <fis a> <e gis>}
  \times 2/3 {<e g!>8 fis g} \times 2/3 {a b cis}
  \times 2/3 {d8 cis d} \times 2/3 {e16 fis g a b cis} |
}
lifeb_guitar = {
  \start_lifeb \change Staff = "bombarde"
  s1*10
  \clef "treble"
  \lauren
  r2 r16 d' e' g' c'' d'' e'' g''
  f''2. a''8. bes''32 a''
  g''2 r
}
lifeb_harp = \relative c' {
  \start_lifeb \change Staff = "positiv"
  s1*8
  \clef "treble"
  \colin
  r2 \times 2/3 { r8 c e f a c }
  \repeat unfold 4 { d16 bes f d }
  \repeat unfold 4 { e' c g e }
  \repeat unfold 4 { f' d bes f }
  \repeat unfold 4 { g' e c g }
  \repeat unfold 2 {
    \times 2/3 {a'16 fis d a fis d} \times 2/3 {a d fis a d fis}
  }
  \times 2/3 {b16 a e a, e cis} \times 2/3 {a cis e a e' a}
  \times 2/3 {b16 a e a, e cis} \times 2/3 {e fis g a b cis} |
}

adventure_strings = \relative c' {
  \start_adventure \change Staff = "swell"
  \lauren
  d''8. d,,16 d8 d e fis g8. g16 g8 g a b
  a8. fis16 fis8 fis g a e fis g a b cis
  d8. d,16 d8 d e fis g8. g16 g8 g a b
  a8. fis16 fis8 fis g a e fis g a b cis
  d r4 r4. | s1*6/8*3 |
  \colin
  bes8. bes16 bes8 bes c d es8. es16 es8 es f g |
  f8. d16 d8 d es f <c es> <d f> <es g> <f a> <g bes> <a c> |
  <a d>8 r fis, fis g a b4 a8~ a d cis |
  b4 cis8 a d a fis4 g8 e4. |
  fis4.~ fis8 g a b4 a8~ a g fis |
  g fis e16 d e4. d e |
  R1*6/8*17 |
  d2. d |
  d4. d d e |
  fis4 fis8 fis g a b4 a8~ a g fis |
  g fis e16 d e4. <fis d g,> <e cis a> |
  R1*6/8*2 |
  r1*6/8 bes4. c as g |
  <<
    { \voiceOne
      c2. d8 es f f es d |
      es8 f g g f es c4. d |
    }
    \new Voice \relative c { \voiceTwo
      \colin
      f2. g g a |
      bes8 c d d c bes bes2. |
      as8 g as a gis a bes2. as |
    }
  >>
}
adventure_strings_ii = \relative c' {
  \start_adventure \change Staff = "choir"
  s1*6/8*16 |
  \lauren r4 fis8 fis g a b4 a8~ a d cis |
  b4 cis8 a d a' fis4 g8 e4. |
  r4 d8 d e fis g4 fis8~ fis g a |
  b4 a8~ a b cis d4 g,8 e'4. |
  <cis a>4 <d b> d, <a' fis>2. |
  <cis a>4 <d b> d, <a' fis> <g e> <fis d> <e cis>4. <fis d>4 r8 |
  <cis' a>4 <d b> <e cis> <d a>2. |
  <cis a>4 <d b> d, g fis e |
  <<
    { \voiceOne
      d8 s4 d,8 e fis g8. g16 g8 g a b |
      a8. fis16 fis8 fis g a e fis g a b cis |
      d8
    }
    \new Voice \relative c' { \voiceTwo
      \lauren
      d8. d16 d8 d4 d8 d8. d16 d8 d4 cis8 |
      d8. d16 d8 d d d d d d d d cis |
      d8
    }
  >> \oneVoice
  r16 fis, fis8 fis g a b8. b16 b8 b cis d |
  d8. fis16 fis8 fis g a e fis g a b cis
  d8 r fis,,8 fis g a b4 a8~ a d cis |
  b4 cis8 a d a' fis4 g8 e4. |
  r4 d8 d e fis g4 fis8~ fis g a |
  b4 a8~ a b cis d4 g,8 e'4. |
  <bes d>8 <c e> <d f> q <c e> <bes d> <a c>2. |
  <as c>8 <bes d> <c es> q <bes d> <as c> <g bes>4. <as c> |
  <f as>4. <es g> \breathe
  f,8 g as as g f f4. d'~ d es |
  a,8 bes c c bes a bes4. fis'?~ fis8 g fis g as g |
  as8 g as a gis a bes bes, c d es f |
  ges?8 as bes c des es | des r4 r4. |
  r4. r8 des, c | bes4 c8 as des as' |
  f4 ges8 es4 r8 |
  r4 \ottava 1 des8 des es f |
  ges4 f ges8 as | bes4 as bes8 c |
  des4 as8 es'8 \ottava 0
}
adventure_counter_strings = \relative c {
  \start_adventure \change Staff = "great"
  s1*6/8*16
  \clef "bass"
  \colin
  \ottava #1
  \set Staff.ottavation = #"play on Swell"
  \set Voice.middleCPosition = #6
  r4. d8 e fis g fis e16 d a'4. |
  b4. a g a |
  a2. g4. a |
  b4. a b a |
  b2. a b g a4. a4 r8 |
  b2. a g |
  \unset Staff.ottavation
  \unset Voice.middleCPosition
  \ottava #0
  <g e>4 <a fis> <g e> |
  <fis d>8 r4 r4. |
  s1*6/8*7 |
  r4
  \ottava #1
  \set Staff.ottavation = #"play on Swell"
  \set Voice.middleCPosition = #6
  d8 d e fis g fis e fis4. |
  b4. a g a |
  <fis a>2. <g b>4. <fis a> |
  <g b>4. <fis a>
  \unset Staff.ottavation
  \unset Voice.middleCPosition
  \ottava #0
}
adventure_reeds = \relative c'' {
  \start_adventure \change Staff = "choir"
  \lauren
  R1*6/8 r4. e8 fis g |
  fis8 r4 r4. r4. r4 \times 2/3 { a,16 b cis } |
  d8 r4 r4. r4. e8 fis g |
  fis8 r4 r4. R1*6/8 |
  bes,8. bes16 bes8 bes c d
  es8. es16 es8 es f g |
  f8. d16 d8 d es f
  c8 d es f g a |
  bes8.
}
adventure_brass = \relative c' {
  \start_adventure \change Staff = "great"
  s1*6/8*4 |
  \colin
  d8. d16 d8 d e fis g8. g16 g8 g a b |
  a8. fis16 fis8 fis g a e fis g a b cis |
  d8 r4 r4. |
  R1*6/8*3 |
  r4. <bes, f'> <bes es> <bes es> |
  <bes d f>4. <bes c f> R2. |
}
adventure_brass_ia = \relative c' {
  \start_adventure \change Staff = "great"
  s1*6/8*37 |
  \colin
  <d fis,>8. q16 q8 q <e g,> <fis a,> <g b,>8. q16 q8 q <a cis,> <b d,> |
  <a d,>8. <fis d>16 q8 q <g e> <a fis> <e g,> <fis a,> <g b,> <a cis,> <b d,> <cis e,> |
  <d fis,>8
}
adventure_brass_ii = \relative c {
  \start_adventure \change Staff = "great"
  \colin
  s1*6/8*48 |
  fis4. g |
  R1*6/8 |
  \lauren
  a4 <a c> <c f> <c es>2.~ |
  <c es>2. <bes d>4. <c es> \breathe |
}
adventure_brass_iii = \relative c' {
  \start_adventure \change Staff = "great"
  s1*6/8*63 |
  \clef "treble"
  \colin
  r4 <c des f>8 q <des es ges> <des f as> <des es ges bes> r <c des f as>~ q4. |
}
adventure_horn = \relative c' {
  \start_adventure \change Staff = "positiv"
  \clef "treble"
  \colin
  <d d'>8 r d r d r d r d r d r |
  d8 r d r d r d r d r cis r |
  d8 r d r d r d r d r d r |
  d8 r d r d r a r a r a r |
  bes8. d16 d8 d es f g8. g16 g8 g a bes |
  bes8. f16 f8 f g a es f g a bes c |
  d8.
}
adventure_horn_ii = \relative c' {
  \start_adventure \change Staff = "positiv"
  s1*6/8*49 |
  \colin
  \times 6/4 {r8 bes-. d-. f-.} \times 6/4 {r8 a,-. c-. f-.} |
  \times 6/4 {r8 as,-. c-. es-.} \times 6/4 {r8 bes-. r c-.} |
  \times 6/4 {r8 d-. r es-.}
}
adventure_bass = {
  \start_adventure \change Staff = "pedal"
  \colin
  d4 r8 r4. R1*6/8*3 |
  d4 r8 r4. R1*6/8 d4 r8 r4. a,4 a, a, |
  bes,4 r8 r4. R1*6/8 |
  bes,4 r8 r4. bes,4 r8 r4 f8 |
  bes,4.-- g-- c-- f-- \lauren bes-- g-- c'8 bes a g f e |
  d2.~ d g4. fis \colin b, cis |
  d2.~ d~ d b,4. cis |
  g,2. fis, g, e, a,4. d4 r8 |
  g,2. fis, b, cis4 d a, |
  d4 r8 r4. R1*6/8 |
  R1*6/8 r4. r8 a, a, |
  d4 r8 r4. r4. r8 a, a, |
  d4 r8 r4. a,4 a, a, |
  d2.~ d g4. fis b, cis |
  d2.~ d~ d b,4. cis |
  bes,4-. r8 r4. a,4-. r8 r4. as,4-. r8 r4. g,-. as,-. |
  bes,-. c-. as,2. b,! c |
  fis,? g, des c4. b,! bes,2. as, |
  des4 r8 r4. R1*6/8*3 |
  des2.~ des~ des bes,4. as, |
}

home_melody = \relative c'' {
  \start_adventure \change Staff = "swell"
  s1*6/8*70 |
  \lauren
  r4. r8 des c |
  des4.~ des8 des c des4 es8 es4 f8 |
  f2.~ f4.~ f8 des c |
  des4.~ des8 bes c des4 f es8 des |
  es2.~ es4. r8 des c |
  des4. as8 des as' f4 ges8 es4 des8 |
  f2.~ f4.~ f8 bes, c |
  des4 f8 f ges as bes4 ges f |
  es4 es8 es f ges as4 f es |
  des4 f8 f ges as bes4 ges f |
  es2.~ es4.~ es8 des c |
  des8
}
home_bass = {
  \start_home \change Staff = "pedal"
  \colin
  des2. bes,4. ges, f,2. bes, |
  es,2. f,4-- f,-- ges,-- as,2.~ as,4. as,8-. as,-. as,-. |
  des2. bes,4. c a,!2. bes, |
  ges,4.~ ges,8 ges,-. f,-. es,4.~ es,8 es,-. es,-. |
  as,4.~ as,8 as,-. ges,-. f,4.~ f,8 f,-. f,-. |
  bes,4.^"+reed?"~ bes,8 bes,-. bes,-. es,4.~ es,8 f,-. ges,-. |
  as,4. as,8-. as,-. as,-. as,-. as,-. as,-. as,-. as,-. as,-. |
}
home_chords = \relative c'' {
  \start_home \change Staff = "great"
  \colin
  << {\voiceOne
    as4.~ as as~ as |
    as4.~ as4 des,8 des es f f ges as |
    bes4. ges8 ges ges <as es>4_- <as es>_- es_- |
    <as es>2.~ q4. r |
    r4 es8 es f r ges4. as |
    f2.~ f4. r |
    r4. des8 es f ges2. |
    as4 r8 c, des es f4 f es |
    f8 r4 r4. es4. ges8 f4 |
    <as es>2. q2. |
    f8
  } \new Voice \relative c' {\voiceTwo \colin
    es8 f es es f r des es es es ges f |
    des8 es es es f bes, bes c des des es f |
    ges4. r4. s2. |
    des4. c bes c |
    des2. des4. es |
    c4. r des4. r8 des des |
    des8 des des des des des des des des des des des |
    des8 des des c c c c[ c] c[ c] c[ c] |
    des8 des des des des des des des des des des des |
    des8 des des c c c bes bes bes as as as |
    as8
  } >>
}
home_strings = \relative c'' {
  \start_home \change Staff = "choir"
  \lauren
  s1*6/8*2 |
  r8 des c des r4 |
  s1*6/8*7 |
  r4. f,8 c' es <es c> <des bes> <c a> <des bes> r4 |
}
home_extra_low_brass = \relative c' {
  \start_home \change Staff = "positiv"
  s1*6/8*14 |
  \lauren
  r4 es8 es f ges as4-- f-- es-- |
  des8 r4 r4.
}

celebration_bgstrings = \relative c' {
  \start_celebration \change Staff = "choir"
  \lauren
  cis!4. cis |
  \repeat unfold 7 {cis4. cis |}
}
celebration_flute = \relative c' {
  \start_celebration \change Staff = "choir"
  s1*6/8*8 |
  \lauren r4.^"CH(4)??" e8 fis a a b a b cis e |
  fis8 r4 cis8 e fis fis e fis a b fis |
  a8 r fis e cis cis r a cis b a a |
  r4 e8 fis a b cis e fis e cis b |
  a r4 r4. |
  r4 a8 a b cis |
  d8 r d r d r e r e r e r |
  a8 r4 r4. R1*6/8*3 |
  r4 e8 e fis gis a r a r a r |
  b,8 r b r b r gis' r gis r gis r |
  a8 r4 r4. R1*6/8*3 |
  r4 e8 e fis e e r a, r e' r |
  d8 r d r d r b r b r b r |
  a8 r4 r4. R1*6/8*3 |
  R1*6/8 a8 r a r b8 cis |
  b8 r b r fis r gis r gis r b r |
  a8 r4 r4.
}
celebration_other_flute = \relative c'' {
  \start_celebration \change Staff = "bombarde"
  s1*6/8*36 |
  \lauren
  r4. r8 e fis | a b fis a fis e |
  fis8 cis e e cis b | cis b a b a fis |
  e4. r | R1*6/8*3 |
  r4 \appoggiatura b''16 cis8 a b4 | a8 fis e fis e cis |
  fis8 r fis r cis b | cis b a fis a b | a8 r4 r4. |
}
celebration_strings = \relative c' {
  \start_celebration \change Staff = "swell"
  R1*6/8*2 |
  \clef "bass"
  \colin r4 <a cis,>8 q <b d,> <cis e,> <d fis,> r4 <b gis>8 r4 |
  <a e>8 r q <cis e,> <d fis,> <e gis,> <fis a,> r4 <e gis,>8 r4 |
  \clef "treble"
  << {\voiceOne
    fis8 r4 d8 fis a gis e gis b e gis | a
  } \new Voice \relative c' {\voiceTwo \colin
    a8 fis a d4. b8 gis b e a, b | cis
  } >> \oneVoice
  r4 <a,, cis>4 q8 q4 q8 q4 q8 |
  r4 <a cis>8 q4 q8 q4 q8 q4 q8 |
  r4 <a d>8 q4 q8 q4 q8 q4 q8 |
  r4 <a d>8 q4 q8 <a e'>4 q8 q4 q8 |
  a8 r4 e'8 a, a a r4 e'8 a, a |
  a8 r4 e'8 a, a b r4 e8 b b |
  a8 r4 a''8 e cis a e cis a r4 |
  a8 r a e' fis e b r b e gis a |
  a,8 r4 e'8 a, a a r4 e'8 a, a |
  a8 r4 e'8 a, a b r4 e8 b b |
  a8 r4 a''8 e cis a e cis e a, a |
  a8 r a r a b cis r b r b r |
  a8 r4 e'8 a, a a r4 e'8 a, a |
  a8 r4 e'8 a, a b r4 e8 b b |
  a8 r4 a''8 e cis a e cis e a, a |
  a8 r a d e fis e b e b' e a |
  r4. e,8 a, a a r4 e'8 a, a |
  a8 r4 e'8 a, a b r4 e8 b b |
  a8 r4 a''8 e cis a e cis e a, a |
  <a fis'>8 r q r <a d> r <cis e> r <b e> r q r |

  <a cis>8 \lauren r a a b cis d4 d8 d e fis |
  e4 e8 e d cis d4 d8 d cis b |
  a8 r a a b cis d4 d8 d e fis |
  e4 e8 e d cis d4 d8 d e fis |

  g4^"SW(8)" es8 es f g as4 as8 as bes c |
  bes4 g8 g as bes f g as bes c d |
  es4 es8 es f g as4 as8 as bes c |
  bes4 g8 g as bes f g es f g as |
  bes c, d es f g as4 bes c d es f |
  es
}
celebration_strings_ii = \relative c'' {
  \start_celebration \change Staff = "choir"
  s1*6/8*51 |
  \lauren
  r8 a a a gis gis |
  a2.~ a4.~ a~ a gis a2. |
  bes2.~ bes~ bes4. es~ es d |
  es2.~ es~ es f~ f~ f~ f |
}
celebration_melody_ii = \relative c'' {
  \start_celebration \change Staff = "swell"
  s1*6/8*68 |
  \lauren
  r4. r8 es d |
  es8 r g, g as bes c4 bes es8 d |
  c4 d8 bes es bes' g4 as8 f4. |
  r4 es8 es f g as4 g as8 bes |
  c4 bes c8 d es4 as,8 f'4. |
  <bes, d>4 <c es> es, <g bes>2. |
  <bes d>4 <c es> <c, es> <g' bes> <f as> <es g> |
  <d f>4. <es g>4 r8 |
  <bes' d>4 <c es> <d f> <bes es>2. |
  <bes d>4 <c es> es,8 as as4 g f |
  es8 r4 r4.
}
celebration_brass = \relative c {
  \start_celebration \change Staff = "great"
  s1*6/8*48 |
  \colin
  s8 r4 r4. |
  \clef "bass"
  fis4. r gis r fis r |
  e4. r fis r gis r fis r |
  << {\voiceOne
    g4. r^"GR(8)" as r g r <as c> r |
    <bes, bes'>2.~ q~ q~ q~ q |
   }
   \new Voice {\voiceTwo \colin
    bes2.~ bes~ bes~ bes |
    g'2. as g as g |
   }
  >> \oneVoice
  \clef "treble"
  <<
    {\voiceOne c4 d es f g as}
    \new Voice {\voiceTwo \colin bes,2. bes4 bes bes}
  >> \oneVoice
  <es g>8 r q <g bes> <f as> <es g> <d f> <es g> <d f> <es g> as, bes |
  <g bes>2. <as c>4. <g bes> <es as c> <es g bes> <as c> <bes d> |
  <bes es>2. c4. d es d <as es'> <bes f'> |
  << {\voiceTwo
    c2. bes c as bes4. g |
    c2. bes as |
  } \new Voice {\voiceOne \colin
    r4. es' r es r es bes'4 as g f4. g |
    r4. es r es r c4 es8 <d f>4 <es g> <d f> |
  } >> \oneVoice
  es8
}
celebration_hits = {
  \start_celebration \change Staff = "great"
  \clef "treble"
  R1*6/8*16 |
  \lauren
  <cis' e' a'>8 r4 r4. | R1*6/8*3 |
  <cis' e' a'>8 r4 r4. | R1*6/8*1 |
  r4 <d' fis' a'>8 r4. | <b e' gis'>8 r4 r4 <cis' e' a'>8 |
  R1*6/8*4 |
  <cis' e' a'>8 r4 r4. | R1*6/8*1 |
  r4. <d' fis' a'>8 r4 | r4 <b e' gis'>8 r4. |

  <cis' e' a'>8 r4 r4. | R1*6/8*3 |
  <cis' e' a'>8 r4 r4. | R1*6/8*1 |
  r4 <d' fis' a'>8 r4. | <b e' gis'>8 r4 r4 <cis' e' a'>8 |
  R1*6/8*4 |
  <cis' e' a'>8 r4 r4. | R1*6/8*1 |
  r4. <d' fis' a'>8 r4 | r4 <b e' gis'>8 r4. |
  <cis' e' a'>8
}
celebration_electricbass = {
  \start_celebration \change Staff = "positiv"
  R1*6/8*12 |
  \clef "treble"
  \colin
  r4. r8 e' fis' a' b' a' r4. |
  r4 d''8~ d''4 b'8 r a' fis' e' cis' b |

  a8 r4 r4. \clef "bass" r8 e fis a fis e |
  d8 r4 d8 a, d e r4 e8 b, e, |
  a,8 r4 r4. r8 e fis a fis e |
  d8 r4 d8 a, d e b, e, e e, a, |
  r2. r8 e fis a fis e |
  d8 r4 d8 a, d e r e r e, r |
  a,8 r4 r4. r8 e fis a fis e |
  d8 r d r d, r e, r e r e, r |

  a,8 r4 a8 r4 r8 e fis a fis e |
  d8 r4 d8 a, d e r4 e8 b, e, |
  a,8 r4 r4. r8 e fis a fis e |
  d8 a, d, d r a, e r b, e, e, a, |
  r2. r8 e fis a fis e |
  d8 r d~ d a, d e r e e, b, a, |
  a,8 r4 r4. r8 a fis e d a, |
  d8 r d r a, d e r e r b, e, |
  a,

}
celebration_bass = {
  \start_celebration \change Staff = "pedal"
  \colin des4 r8 r4. R1*6/8*7 |
  R1*6/8*2 r4 fis,8~ fis,4.~ fis,2. |
  d8 r4 r4. R1*6/8 r4 b,8~ b,4. e4 r8 r4. |
  a,8 r4 r4. R1*6/8*3 |
  a,8 r4 r4. R1*6/8*3 |
  a,8 r4 r4. R1*6/8*3 |
  a,8 r4 r4. R1*6/8*3 |
  a,8 r4 r4. R1*6/8*3 |
  a,8 r4 r4. R1*6/8*3 |
  a,8 r4 r4. R1*6/8*3 |
  a,8 r4 r4. R1*6/8*3 |
  \repeat unfold 8 {a,4. r} |
  \repeat unfold 9 {bes,4. r} |
  bes,4 bes, bes, bes, bes, bes, |
  es8 r4 r4. | r4. r8 as, bes, |
  es2.~ es as4. g c d |
  es2.~ es~ es c4. d |
  as,2. g, as, f, bes,4. es4 r8 |
  as,2. g, c d4 es bes, |
}

meaning_scales = \relative c'' {
  \start_meaning \change Staff = "choir"
  s1*4 s2 |
  R1*2 |
  \colin
  r2 <bes g'>8 <as f'> <g es'> <f d'> |
  <es c'>8 <d bes'> <c as'> <bes g'> <as f'> <g es'> r4 |
  R1*2 |
  r2 <c' as'>8 <bes g'> <as f'> <g es'> |
  <f d'>8 <es c'> <d bes'> <c as'> <bes g'> <as f'> r4 |
  R1*2 |
  <b' g'>8 <a f'> <g es'> <f d'>
  r8 \times 2/3 {c16 d es} \times 2/3 {f g a} \times 2/3 {b c d} |
  es4. g8 \times 2/3 {g4 as bes} |
  c4 as g4. f8 |
  es4. es8 \times 2/3 {es4 f g} |
  f2 r | <g,, bes es>4
}
meaning_melody = \relative c' {
  \start_meaning \change Staff = "great"
  \colin
  s1*1 | r2. c16 d es f |
  g16 as bes bes, c d es f g as bes c d es es, f |
  g16 as bes c d es f g d es f g as bes c d |
  \change Staff = "positiv"
  \lauren
  es,,4 d |
  es2. es8 d es4 f f4.. g16 g1~ g2 r4 es8 d |
  es2. c8 d es4 g f4. es8 f1~ f2 r4 es8 d |
  es2~ es8 bes es bes' g4. as8 f4. es8 |
  g1 r4. g8 \times 2/3 { g4 as bes } |
  c4 as g4. f8 es4. es8 \times 2/3 { es4 f g } |
  f4 r f2 |
}
meaning_chords = \relative c' {
  \start_meaning \change Staff = "great"
  \colin
  s1*4 | es4 d |
  <as c>1 q2 <bes d>4.. <bes es>16 q1~ q2 r2 |
  <g c>1 <as c>2 q <bes d>1~ q2 r2 |
  <g bes>2. r4 <as es'>2 <bes d> |
  <g d'>1
  << {\voiceTwo <g c>1 <f as>2 <g b> |}
     \new Voice
     {\voiceOne \colin es'4. es8 \times 2/3 {es4 f g} as4 f d2 |}
  >> \oneVoice
  <g, c>2 <g bes> <f c'> <f bes d> |
}
meaning_bass = {
  \start_meaning \change Staff = "pedal"
  \colin
  es,1~ es,~ es,~ es,~ es,8 r r4 |
  as,1 f,2 bes,4.. es,16 es,1^"+Z"~ es,2 r4 es8-- d-- |
  c1 f,2 as, bes,1~ bes,2 r4 bes,8-- bes,-- |
  es,1^"-Z" c2 d b,1^"+Z" c2^"-Z" c |
  f,2 g, c bes, a,8 as, f,4 bes,2 |
}
meaning_rhythm = \relative c'' {
  \start_meaning \change Staff = "positiv"
  \clef "treble"
  \lauren
  \repeat unfold 16 { \times 4/6 {es16 d c bes as g} } |
}
meaning_trumpet = \relative c'' {
  \start_meaning s1*4 s2 s1*6 \change Staff = "bombarde"
  \lauren
  r2 r4 \times 2/3 { r8 bes bes } bes'1~
  bes2~ bes8 as g f es2 r |
  R1*3 r4. es8 \times 2/3 {es4 f g} |
  f4 r f2\trill | es4 s4_"-chamade"
}

finale_bass = {
  \start_finale \change Staff = "pedal"
  \colin
  ees,4 r r r8 ees,4 ees, r r8
  aes,4 r r r8 aes,4 aes, bes, r8
  ees,4 r r r8 ees,4 ees, r r8
  aes,4 r r r8 aes,4 bes, ees, r8
  c4 r r r8 f,4 f, r r8
  bes,4 r r r8 ees,4 ees, r r8
  c4 r r r8 f,4 f, g, r8
  c4 bes, aes, r8 bes,4 bes, ees, r8
  e,4 r r r8 e,4 e, r r8
  a,4 r r r8 a,4 cis b, r8
  e,4 r r r8 e,4 e, r r8
  a,4 r r r8 cis4 b, e, r8
  cis4 cis cis cis8 fis,4 fis, fis, fis,8
  b,4 b, b, b,8 e,4 e, e, e,8
  cis4 cis cis cis8 fis,4 fis, gis, gis,8
  cis4 b, a, r8 b,4 b, cis r8
  cis4 b, a, r8 b,4 b, e, r8
  e,4 r r r8 e,4 e, r r8
  b,4 r r r8 b,4 b, r r8
  e,4 r r r8 e,4 e, r r8
  e,4^"+32' Bom" r r r8 \lauren c'8 b a g fis e d
  c2 \colin r8 b,~ b, c~ c b,8 |
  a,2.~ a,4 r8 |
  a,8 r r b, r r e, r r
}
finale_melody = {
  \start_finale \change Staff = "great"
  \lauren
  r4 g' bes' g'8 aes'4 f' g' ees'8 |
  r4 c' ees' c'8 c'4 ees' d' r8 |
  r4 g' bes' g'8 aes'4 f' g' ees'8 |
  r4 c' ees' c'8 ees'4 d' ees' r8 |
  r4 g' aes' bes'8 c''4 aes' g' f'8 |
  r4 f' g' aes'8 bes'4 g' f' ees'8 |
  r4 g' aes' bes'8 c''4 aes' g' f'8 |
  ees'4 ees' f' g'8 f'4 f' ees' r8 |
  r4 gis' b' gis'8 a'4 fis' gis' e'8 |
  r4 cis' e' cis'8 cis'4 e' dis' r8 |
  r4 gis' b' gis'8 a'4 fis' gis' e'8 |
  r4 cis' e' cis'8 e'4 dis' e' r8 |
  r4 gis' a' b'8 cis''4 a' gis' fis'8 |
  r4 fis' gis' a'8 b'4 gis' fis' e'8 |
  r4 gis' a' b'8 cis''4 a' gis' fis'8 |
  e'4 e' fis' gis'8 fis'4 fis' e' r8 |
  gis'4 gis' a' b'8 cis''4 dis'' e'' r8 |
  R1*7/4*2 |
  r4 b e' b'8 gis'4 a' fis' e'8
  r4 g' c'' g''8 e''4 fis'' d'' fis''8
  e''4 r^"+chamade" r8 a'~ a' b'~ b' d'' e'' r r r4. r |
  e''8 r r dis'' r r e'' r r |
}
finale_strings = \relative c' {
  \start_finale \change Staff = "swell"
  \colin
  r4 bes d bes8 c4 bes bes bes8 |
  r4 as c as8 as4 c bes r8 |
  r4 bes d bes8 c4 bes bes bes8 |
  r4 as c as8 c4 bes bes r8 |
  <c es>2.~ q8 <c f>8 r r4 r r8 |
  <bes d>2.~ q8 <bes es>8 r r4 r r8 |
  <c es>2.~ q8 <c f>8 r r4 b r8 |
  c4 c c r8 d4 d es r8 |
  \lauren
  r4 e gis e8 fis4 dis e r8 |
  r4 a, cis a8 a4 cis b r8 |
  r4 e gis e8 fis4 dis e r8 |
  r4 a, cis a8 cis4 b b r8 |
  R1*7/4*3 |
  \colin
  cis4 cis cis r8 b4 b cis r8 |
  gis'4 gis fis gis8 fis4 fis e r8 |
  \lauren <g c>1*7/4 |
  <a d>1*7/4 |
  <b e>1*7/4~ |
  q2. r8 r4 r r r8 |
  R1*5/4 |
  e2.~ e4_"+Tutti" r8 |
  g,8 r r fis r r gis r r |
}
finale_marimba_flutes = \relative c' {
  \start_finale \change Staff = "choir"
  R1*7/4*12 |
  \lauren
  e4 e fis gis8 a8 r r4 r r8 |
  dis,4 dis e fis8 gis8 r r4 r r8 |
  e4 e fis gis8 a8 r r4 r r8 |
  R1*7/4*4 |
  \colin
  r4 r r r8 <b d e>4 q q r8 |
  r4 r r r8 <c e>4 q q r8 |
  R1*5/4 |
  R1*9/8*2 |
}
finale_trumpet = \relative c' {
  \start_finale \change Staff = "bombarde"
  R1*7/4*8
  \colin
  r4 e e r8 e4 e e r8 |
  r4 e e r8 e4 e dis r8 |
  r4 e e r8 e4 e e r8 |
  r4 e e r8 e4 dis e r8 |
  e2.~ e8 fis r r4 r4. |
  dis2.~ dis8 e r r4 r4. |
  e2.~ e8 fis r r4 r4. |
  R1*7/4 |
  \lauren
  e4 e fis gis8 fis4 fis e r8 |
  R1*7/4*4 |
  R1*5/4*1 |
  R1*9/8*2 |
}
finale_reeds = {
  \start_finale \change Staff = "positiv"
  \clef "treble"
  R1*7/4*4 |
  \colin
  r4 r r8 f'16 g' as' bes' c''8 r r4 r r8 |
  r4 r r8 es'16 f' g' as' bes'8 r r4 r r8 |
  r4 r r8 f'16 g' as' bes' c''8 r r4 r r8 |
  r4 r r r8 \times 2/3 {f8 g as} \times 2/3 {bes c' d'} es'4 r8 |
  R1*7/4*4 |
  r4 r r8 fis'16 gis' a' b' cis''8 r r4 r r8 |
  r4 r r8 e'16 fis' gis' a' b'8 r r4 r r8 |
  r4 r r8 fis'16 gis' a' b' cis''8 r r4 r r8 |
  r4 r r4 r8 \times 2/3 {fis8 gis a} \times 2/3 {b cis' dis'} e'4 r8 |
  r4 r r4 r8 r2 r4 b'8 |
  e''8[ c'' b' g'] b'[ c'' e''] g''[ e'' c'' b'] c''[ e'' g'']
  a''[ fis'' d'' cis''?] d''[ fis'' a''] b''[ a'' fis'' d''] fis''[ a'' b'']
  b''4 r r r8 r4 r r r8 |
  R1*7/4 |
  r4 r r r8 b' g' b' |
  c''8 \clef "bass" a, c e a c' \clef "treble" e' a' c'' |
  e''8 r r dis'' r r e'' r r |
}

everything = <<
  \chaosa_bass
  \chaosa_hits
  \chaosa_strings
  \chaosa_brass
  \chaosa_low
  \chaosa_trumpet
  \chaosa_flutes

  \space_arp
  \space_bass
  \space_oboe
  \space_strings
  \space_strings_ii
  \space_quiet_background
  \space_clarinet

  \lifea_arp
  \lifea_marimba
  \lifea_bass
  \lifea_melody
  \lifea_strings
  \lifea_brass
  \lifea_low_strings

  \lifeb_bass
  \lifeb_flute
  \lifeb_flute_ii
  \lifeb_brass
  \lifeb_strings
  \lifeb_guitar
  \lifeb_harp

  \adventure_strings
  \adventure_strings_ii
  \adventure_counter_strings
  \adventure_reeds
  \adventure_brass
  \adventure_brass_ia
  \adventure_brass_ii
  \adventure_brass_iii
  \adventure_horn
  \adventure_horn_ii
  \adventure_bass

  \home_melody
  \home_bass
  \home_chords
  \home_strings
  \home_extra_low_brass

  \celebration_bgstrings
  \celebration_flute
  \celebration_other_flute
  \celebration_strings
  \celebration_strings_ii
  \celebration_melody_ii
  \celebration_brass
  \celebration_hits
  \celebration_electricbass
  \celebration_bass

  \meaning_scales
  \meaning_melody
  \meaning_chords
  \meaning_rhythm
  \meaning_trumpet
  \meaning_bass

  \finale_bass
  \finale_melody
  \finale_strings
  \finale_marimba_flutes
  \finale_trumpet
  \finale_reeds
>>
