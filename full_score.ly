% vim: sts=2 sw=2 expandtab
\version "2.18.2"
\include "parts.ly"

#(set-global-staff-size 15)
\paper {
  #(set-paper-size "letter")
  bottom-margin = 0.5\in
  left-margin = 0.5\in
}

\header {
  title = "Reflections of Earth"
  subtitle = "for two organists"
  composer = "Composed by Gavin Greenaway"
  arranger = "Arranged by Colin Chan"
  tagline = ""
}

\score {
  <<
    \new StaffGroup <<
      \new Staff = "bombarde" \with {
        instrumentName = "Bombarde"
        shortInstrumentName = "Bom"
        midiInstrument = "church organ"
      } << \global \everything >>
      \new Staff = "choir" \with {
        instrumentName = "Choir"
        shortInstrumentName = "Ch"
        midiInstrument = "church organ"
      } \global
      \new Staff = "swell" \with {
        instrumentName = "Swell"
        shortInstrumentName = "Sw"
        midiInstrument = "church organ"
      } \global
      \new Staff = "great" \with {
        instrumentName = "Great"
        shortInstrumentName = "Gr"
        midiInstrument = "church organ"
      } \global
      \new Staff = "positiv" \with {
        instrumentName = "Positiv"
        shortInstrumentName = "Pos"
        midiInstrument = "church organ"
      } \global
    >>
    \new Staff = "pedal" \with {
      instrumentName = "Pedal"
      shortInstrumentName = "Pd"
      midiInstrument = "church organ"
    } \global
  >>
  \layout {
    \context {
      \Staff
      % Uncomment these later (they make compilation slower)
      %\RemoveEmptyStaves
      %\override VerticalAxisGroup.remove-first = ##t
      \override AccidentalPlacement.padding = #-0.15
    }
  }
  \midi {}
}
