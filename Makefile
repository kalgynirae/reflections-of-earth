.PHONY: final

full_score.pdf: full_score.ly parts.ly
	lilypond --loglevel=PROGRESS full_score.ly

final:
	lilypond -dno-point-and-click --loglevel=PROGRESS -o full_score-$$(date +%Y%m%d) full_score.ly
